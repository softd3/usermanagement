/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.usermanagement;

/**
 *
 * @author Aritsu
 */
public class TestUserService {
    public static void main(String[] args) {
        //test add
        UserService.addUser("user2","password");
        
        System.out.println(UserService.getUsers());
        UserService.addUser(new User("user3","password"));
        System.out.println(UserService.getUsers());
        
        // test update
        User user3 = UserService.getUser(3);
        System.out.println(user3);
        user3.setPassword("1234");
        UserService.updateUser(3, user3);
        System.out.println(UserService.getUsers());
        
        //test delete
        UserService.delUser(user3);
        System.out.println(UserService.getUsers());
        
        //test login
        System.out.println(UserService.login("admin", "password"));
    }
}
